import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div class="myForm">  
        <div class="row">           
          <div class="col-md-6">       
            <form>            
              <br/>
              <div class="row form-group">
                <div class="col-md-4">First Name</div>
                <div class="col-md-6"><input type="text" name="fName"
                  placeholder = "First Name" required
                  class = "form-control"></div>
              </div>
              <div class="row form-group">
                <div class="col-md-4">Last Name</div>
                <div class="col-md-6"><input type="text" name="lName"
                  placeholder = "Last Name" required
                  class = "form-control"></div>
              </div>
                <div class="row form-group">
                  <div class="col-md-4">Email</div>
                  <div class="col-md-6"><input type="text" name="Email"
                    placeholder = "Email" emailvalidator required
                    class = "form-control"></div>
                </div>
                <div class="row form-group">
                  <div class="col-md-4">Gender</div>
                  <div class="col-md-6">
                  <input type="radio" name="gender" value="Male" required />Male
                  <input type="radio" name="gender" value="Female" required />Female
                </div>
              </div>
              <div class="row">
                <div class="col-md-4">Hair Color</div>
                <div class="col-md-6">
                  <input type="radio" name="hairColor" required value="Brown" />Brown
                  <input type="radio" name="hairColor" required value="Black" />Black
                  <input type="radio" name="hairColor" required value="Golden" />Golden
                </div>
              </div>
              <div class="col-md-3 text-center" style="float:left;">
                <br/>
                <input type = "submit"                         
                  name = "submit"
                  class = "btn btn-primary btn-block " value = "Save" />
              </div>
              <div class="col-md-3 text-center" style="float:left;">
                <br/>
                <input type = "button"                         
                  name = "submit"
                  class = "btn btn-primary btn-block" value = "Cancel" />
              </div>
            </form>              
          </div>         
        </div>
      </div>
      <div class=" myForm">
          <div class="row form-group">
              <div class="col-md-12"> <span style="color:red; padding-bottom: 10px;"> {{MSG}}</span></div>
          </div>
          <div class="row"> 
          <div>
          <div class="col-md-12 text-center float-left" >
              <div class="col-md-6 text-center" style="float:left;">
                <input class="form-control" type="text"
                      name="keyword"
                      placeholder="keyword" required />
              </div>
                <div class="col-md-2 text-center" style="float:left;">
                  <button class="btn btn-primary btn-block " >Search</button>
                </div>
              
              </div>
              <div class="col-md-12 text-center float-left">
                <div class="col-md-3 text-center" style="float:left;">
                  <span>Gender</span>
                  <br/>
                  <input type="radio" name="genderSerach" />Male           
                  <input type="radio" name="genderSerach" />Female 
                  <input type="radio" name="genderSerach" checked />All          
                </div>
                  <div class="col-md-4 text-center" style="float:left;">
                    <span>Hair Color</span>
                    <br/>
                    <input type="checkbox"  name="hairColor" />Brown
                    <input type="checkbox" name="hairColor" />Black
                    <input type="checkbox" name="hairColor" />Golden
                  </div>
                
                </div>
                </div>
              <br/>
              <br/>
              <div class="col-md-12" >
                <a class="float-right" style="cursor: pointer;" ><strong><u>Add New</u></strong></a>
            </div>
          <div class="col-md-12">
              <div class="row">
                  
                    <div class="table-responsive" ng-if="userlist.length>0">
                  <table class="table table-striped " id="tbluserlist" >
                          <thead>
                          <tr>
                          <th width="20%">FirstName</th>
                          <th width="25%">LastName</th>
                          <th width="22%" >Email</th>
                          <th width="20%" >Gender</th>
                          <th width="20%" >Hair Color</th>
                          <th width="22%" >Action</th>
                          </tr>
                          </thead>
                          <tbody >
                          <tr >
                          <td> </td>
                          <td></td>
                          <td></td>
                          
                          <td></td>
                          <td></td>
                          <td>
                            <a style="color: grey; cursor: pointer;"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;&nbsp;

                            <a style="color: grey; cursor: pointer;"><i class="glyphicon glyphicon-trash"></i> </a>
                          </td>
                          </tr>
                          </tbody>
                          </table>
                        </div>
                      
                          <div  class="col-md-12">
                              <div class="alert alert-danger text-center">
                                  <strong>No record found.</strong> 
                                  
                              </div>
                        </div>
                      
                  </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
